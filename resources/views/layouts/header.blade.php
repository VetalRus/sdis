<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="robots" content="noindex,nofollow">
        <meta name="theme-color" content="#fff"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
<!--         <link rel="shortcut icon" href="img/favicons/16.png"/>
        <link rel="apple-touch-icon" href="img/favicons/60.png"/>
        <link rel="apple-touch-icon" sizes="76x76" href="img/favicons/76.png"/>
        <link rel="apple-touch-icon" sizes="120x120" href="img/favicons/120.png"/>
        <link rel="apple-touch-icon" sizes="152x152" href="img/favicons/152.png"/> -->
        <title>Sdis</title>
        <meta name="description" content="">
        <meta property="og:title" content="Sdis"/>
		<meta property="og:description" content=""/>
<!-- 		<meta property="og:image" content="img/favicons/opengraph.png"/> -->
        <!--[if IE]>
            <script>
                document.createElement('header');
                document.createElement('nav');
                document.createElement('main');
                document.createElement('section');
                document.createElement('article');
                document.createElement('aside');
                document.createElement('footer');
            </script>
        <![endif]-->

        <link href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.6/css/swiper.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    </head>
    <body>
        <header class="header">
            <div class="info">
                <div class="wrap">
                    <a href="mailto:contact@sdis-chamberonne.ch">contact@sdis-chamberonne.ch</a>
                </div>
                @if (Route::has('login'))
                    <div class="hidden fixed top-0 right-0 mr-3 px-6 py-4 sm:block">
                        @auth
                            <a href="{{ url('/dashboard') }}" class="text-sm text-gray-700 dark:text-gray-500 underline" style="color: white">Admin Panel</a>
                        @else
                            <a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline" style="color: white">Log in |</a>

                            @if (Route::has('register'))
                                <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline" style="color: white">Register</a>
                            @endif
                        @endauth
                    </div>
                @endif
            </div>

            <div class="wrap">
                <div class="flex">
                    <a href="{{ route('home-page') }}" class="logo">
                        <img src="{{ asset('assets/img/logo-main.svg') }}" alt="">
                    </a>
                    <div class="action">
                        <nav class="nav">
                            <ul class="menu">
                                <li class="item">
                                    <a href="" class="link">Présentation</a>
                                    <ul>
                                        <li><a href="formation.php">Sites</a></li>
                                        <li><a href="#">Organisation</a></li>
                                        <li><a href="formation.php">Missions</a></li>
                                        <li><a href="formation.php">Formation</a></li>
                                        <li><a href="{{ route('vehicles-list') }}">Véhicules</a></li>
                                    </ul>
                                </li>
                                <li class="item">
                                    <a href="{{ route('alarmes-list') }}" class="link">Alarmes</a>
                                </li>
                                <li class="item">
                                    <a href="{{ route('actives-list') }}" class="link">Activités & divers</a>
                                </li>
                                <li class="item">
                                    <a href="#" class="link">Contact</a>
                                </li>
                            </ul>
                        </nav>
                        <a href="connexion.php" class="connect">
                            <i class="icon icon-profile"></i>
                            <span>Connexion</span>
                        </a>
                        <div class="hamburger">
                            <span class="line"></span>
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="shadow"></div>
        <div class="m-panel">
            <div class="content">
                <span class="close icon-signs"></span>
                <nav class="nav">
                    <ul class="menu">
                        <li class="item">
                            <a href="" class="link">Présentation</a>
                            <ul>
                                <li><a href="formation.php">Sites</a></li>
                                <li><a href="#">Organisation</a></li>
                                <li><a href="formation.php">Missions</a></li>
                                <li><a href="formation.php">Formation</a></li>
                                <li><a href="vehicles.php">Véhicules</a></li>
                            </ul>
                        </li>
                        <li class="item">
                            <a href="alarmes.php" class="link">Alarmes</a>
                        </li>
                        <li class="item">
                            <a href="activities-drivers.php" class="link">Activités & divers</a>
                        </li>
                        <li class="item">
                            <a href="#" class="link">Contact</a>
                        </li>
                    </ul>
                </nav>
                <a href="connexion.php" class="connect">
                    <i class="icon icon-profile"></i>
                    <span>Connexion</span>
                </a>
            </div>
        </div>


