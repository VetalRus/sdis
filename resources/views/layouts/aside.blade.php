<aside class="aside">
    <div class="cont">
        <div class="info">
            <div class="title">
                <h4>Alarmes 2021</h4>
            </div>
            <div class="info-alarms">
                <div class="atten-info">
                    <p>Nombre de jours :  {{ $allInfo['difference_days'] }}</p>
                    <p>Moyenne annuelle :  {{ $allInfo['count_posts'] }}</p>
                    <p>Une alarme tous les : {{ round($allInfo['frequency_of_events']) }} jours</p>
                </div>
                @foreach($allInfo['count_category'] as $type => $count)
                    <div class="row">
                        <span>{{$type}}</span>
                        <span>: {{$count}}</span>
                    </div>
                @endforeach

            </div>
        </div>
    </div>
</aside>
