@include('layouts/header')

<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/1.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>Activités & divers</h1>
						</div>
						<div class="block-tabs">
							<div class="links-tab cart-tabs">
								<a href="#cart-tabs_01" class="link btn active">Activités</a>
								<a href="#cart-tabs_02" class="link btn">Divers</a>
							</div>
							<div class="table-list wrap-tabs">
                                    <div class="block-list cart-tabs__item cart-tabs__item--active" id="cart-tabs_01">
                                        @foreach($allInfo['posts']['Activity'] as $key => $value)
                                            <div class="table-list">
                                                <div class="title"><h2>{{$key}}</h2></div>
                                                @foreach($value as $post)
                                                    <div class="block-list">
                                                        <a href="#" class="row">
                                                            <span class="number">{{ $post->number }}</span>
                                                            <span class="month">{{ $post->type }}</span>
                                                            <span class="desc">{{ $post->description }}</span>
                                                            <span class="city">{{ $post->commune->title }}</span>
                                                            <span class="date">{{ $post->created_at->format('D') }}</span>
                                                            <span class="date">{{ $post->created_at->format('d.m') }}</span>
                                                            <span class="time">{{ $post->created_at->format('H:m') }}</span>
                                                            @if(isset($post->img))
                                                                <i class="icon icon-picture"></i>
                                                            @endif
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
								<div class="block-list cart-tabs__item" id="cart-tabs_02" style="display: none;">
                                    @foreach($allInfo['posts'] ['Driver'] as $key => $value)
                                        <div class="table-list">
                                            <div class="title"><h2>{{$key}}</h2></div>
                                            @foreach($value as $post)
                                                <div class="block-list">
                                                    <a href="#" class="row">
                                                        <span class="number">{{ $post->number }}</span>
                                                        <span class="month">{{ $post->type }}</span>
                                                        <span class="desc">{{ $post->description }}</span>
                                                        <span class="city">{{ $post->commune->title }}</span>
                                                        <span class="date">{{ $post->created_at->format('D') }}</span>
                                                        <span class="date">{{ $post->created_at->format('d.m') }}</span>
                                                        <span class="time">{{ $post->created_at->format('H:m') }}</span>
                                                        @if(isset($post->img))
                                                            <i class="icon icon-picture"></i>
                                                        @endif
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
								</div>
                            </div>
						</div>
					</div>
                   @include('layouts/aside')
				</div>
			</div>
		</section>
	</main>
    @include('layouts/footer')
</div>


