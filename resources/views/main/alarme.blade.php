@include('layouts/header')

<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/2.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>{{ $allInfo['post']->title }} {{ $allInfo['post']->number }}</h1>
						</div>
						<div class="point-desc">
							<div class="info-list">
								<div class="row">
									<strong>Numéro d’alarme:</strong>
									<span>{{ $allInfo['post']->number }}</span>
								</div>
								<div class="row">
									<strong>Description:</strong>
									<span>{{ $allInfo['post']->description }}</span>
								</div>
								<div class="row">
									<strong>Commune:</strong>
									<span>{{ $allInfo['post']->commune->title }}</span>
								</div>
								<div class="row">
									<strong>Date:</strong>
									<span>{{ $allInfo['post']->created_at->format('D d M Y, H:i') }}</span>
								</div>
								<div class="row">
									<strong>Type:</strong>
									<span>{{ $allInfo['post']->type }}</span>
								</div>
							</div>
							<a href="{{ route('alarmes-list') }}" class="btn">Toutes les alarmes</a>
						</div>
					</div>
                    @include('layouts/aside')
				</div>
			</div>
		</section>
	</main>
    @include('layouts/footer')
</div>


