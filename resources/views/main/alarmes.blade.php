@include('layouts/header')

<div class="wrapper">
	<main>
		<div class="banner mb" style="background-image: url('img/banner/1.jpg')"></div>
		<section class="container">
			<div class="wrap">
				<div class="columns">
					<div class="content">
						<div class="title">
							<h1>Alarmes</h1>
						</div>
                        @foreach($allInfo['posts'] as $key => $value)
						<div class="table-list">
							<div class="title"><h2>{{$key}}</h2></div>
                            @foreach($value as $post)
							<div class="block-list">
								<a href="{{ route('alarm.single', $post->id) }}" class="row">
									<span class="number">{{ $post->number }}</span>
									<span class="month">{{ $post->type }}</span>
									<span class="desc">{{ $post->description }}</span>
									<span class="city">{{ $post->commune->title }}</span>
									<span class="date">{{ $post->created_at->format('D') }}</span>
									<span class="date">{{ $post->created_at->format('d.m') }}</span>
									<span class="time">{{ $post->created_at->format('H:m') }}</span>
                                    @if(isset($post->img))
									    <i class="icon icon-picture"></i>
                                    @endif
								</a>
							</div>
                            @endforeach
						</div>
                        @endforeach
					</div>
                    @include('layouts/aside')
                    <div class="block-archives">
                        <div class="title">
                            <h4>Archives</h4>
                        </div>
                        <ul>
                            @foreach($allInfo['archive'] as $key => $archive)
                                <li>
                                    <a href="#">Alarmes {{$key}}</a>
                                </li>
                            @endforeach

                        </ul>
                    </div>
			</div>
		</section>
	</main>
    @include('layouts/footer')
</div>


