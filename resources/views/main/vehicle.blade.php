@include('layouts/header')

<div class="wrapper">
    <main>
        <div class="banner">
            <img src="{{asset('assets/img/banner/1.jpg')}}" alt="">
        </div>
        <section class="container vehicles-section">
            <div class="wrap">
                <section class="section-text single-text">
                    <div class="title">
                        <h1>Les Véhicules du SDIS</h1>
                    </div>
                    <div class="editor">
                        <p>Le parc du SDIS Chamberonne comprend 9 véhicules et plusieurs engins : 2 véhicules légers « officier de service », 1 tonne-pompe 2000l dernière génération, 1 tonne-pompe 1800l, 1 véhicule lourd d’accompagnement, 2 véhicules mi-lourds d’accompagnement et de transport de personnes, 2 véhicules légers de type jeep, 2 moto-pompes, 1 remorque « canon », 1 remorque « protection respiratoire », 2 remorques « transport de tuyaux », 1 remorque « inondation » et 2 remorques diverses.</p>
                    </div>
                    <div class="block-tabs">
                        <div class="links-tab cart-tabs-vehicle">
                            <a href="#cart-tabs_01" class="link btn active">All</a>
                            <a href="#cart-tabs_02" class="link btn">Caserne 1</a>
                            <a href="#cart-tabs_03" class="link btn">Caserne 2</a>
                        </div>
                    </div>
                </section>
            </div>
        </section>
        <section class="vehicles-posts">
            <div class="wrap">
                <div class="block-list cart-tabs__item cart-tabs__item--active" id="cart-tabs_01">
                    <div class="block-posts flex">
                        @foreach($data['allVehicles'] as $vehicle)
                            <a href="vehicles-chef.php" class="item-posts">
                                <div class="img" style="overflow: hidden">
                                    <img src="{{asset('storage/'.$vehicle->img)}}" alt="" width='100%'>
                                </div>
                                <span class="title-post">{{ $vehicle->title }}</span>
                                <span class="desc">{{ $vehicle->description }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="block-list cart-tabs__item" id="cart-tabs_02" style="display: none;">
                    <div class="block-posts flex">
                        @foreach($data['type1'] as $vehicle)
                            <a href="vehicles-chef.php" class="item-posts">
                                <div class="img" style="overflow: hidden">
                                    <img src="{{asset('storage/'.$vehicle->img)}}" alt="" width='100%'>
                                </div>
                                <span class="title-post">{{ $vehicle->title }}</span>
                                <span class="desc">{{ $vehicle->description }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
                <div class="block-list cart-tabs__item" id="cart-tabs_03" style="display: none;">
                    <div class="block-posts flex">
                        @foreach($data['type2'] as $vehicle)
                            <a href="vehicles-chef.php" class="item-posts">
                                <div class="img" style="overflow: hidden">
                                    <img src="{{asset('storage/'.$vehicle->img)}}" alt="" width='100%'>
                                </div>
                                <span class="title-post">{{ $vehicle->title }}</span>
                                <span class="desc">{{ $vehicle->description }}</span>
                            </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('layouts.footer')
</div>



