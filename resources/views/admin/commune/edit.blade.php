@extends('admin.index')

@section('content')

    <div class="container ">
        <div class="row d-flex justify-content-center">
            <div class="col-6">
                <h2 class="mt-3 text-center"> Edit the commune</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-6">
                <form method="post" action="{{ route('commune.update', $commune->id) }}">
                    @csrf
                    <div class="form-group">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <label for="exampleInputEmail1">Commune's name</label>
                        <input type="text" class="form-control"  name="title" value="{{$commune->title}}">
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection

