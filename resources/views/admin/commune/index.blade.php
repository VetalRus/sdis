@extends('admin.index')

@section('content')

    <div class="container-fluid ">
        <div class="col-12">
            <table class="table table-striped table-dark mt-3">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Created</th>
                <th scope="col">Updated</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($communes as $commune)
            <tr>
                <th scope="row">{{$counter++}}</th>
                <td>{{$commune->title}}</td>
                <td>{{$commune->created_at}}</td>
                <td>{{$commune->updated_at}}</td>
                <td>
                    <a href="{{ route('commune.edit', $commune->id) }}" title="Edit">
                        <button type="button" class="btn btn-warning">Edit</button>
                    </a>

                    <a href="{{ route('commune.destroy', $commune->id) }}" title="Delete">
                        <button type="button" class="btn btn-danger">Delete</button>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            <a href="{{ route('commune.create') }}" type="button" class="btn btn-primary">Create new commune</a>
        </div>
    </div>
@endsection
