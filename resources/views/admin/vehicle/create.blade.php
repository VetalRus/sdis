@extends('admin.index')

@section('content')

<div class="container ">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
        <h2 class="mt-3 text-center"> Create vehicle</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="{{ route('vehicle.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <label for="exampleInputEmail1">Vehicle's name</label>
                        <input type="text" class="form-control"  name="title" placeholder="Enter name">
                    <label for="exampleInputEmail1">Vehicle's description</label>
                        <input type="text" class="form-control"  name="description" placeholder="Enter description">
                    <label for="exampleInputEmail1">Vehicle's type</label>
                    <select name="type" class="form-select" aria-label="Default select example">
                        <option selected>Select vehicle's type</option>
                        <option value="0">Caserm 1</option>
                        <option value="1">Caserm 2</option>
                    </select>
                    <label for="exampleInputImage">Image</label>
                        <input type="file" class="form-control"  name="img">
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection

