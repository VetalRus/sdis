@extends('admin.index')

@section('content')

    <div class="container-fluid ">
        <div class="col-12">
            <table class="table table-striped table-dark mt-3">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
                <th scope="col">Image</th>
                <th scope="col">Created</th>
                <th scope="col">Updated</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($vehicles as $vehicle)
            <tr>
                <th scope="row">{{$counter++}}</th>
                <td>{{$vehicle->title}}</td>
                <td>@if($vehicle->type === 0){{'Caserm 1'}}
                    @else {{'Caserm 2'}}
                    @endif
                </td>
                <td>{{$vehicle->description}}</td>
                <td>
                    <img  style="width: 50px; height: 50px"
                          src="{{ asset('storage/'.$vehicle->img) }}  " alt="image post">
                </td>
                <td>{{$vehicle->created_at}}</td>
                <td>{{$vehicle->updated_at}}</td>
                <td>
                    <a href="{{ route('vehicle.edit', $vehicle->id) }}" title="Edit">
                        <button type="button" class="btn btn-warning">Edit</button>
                    </a>

                    <a href="{{ route('vehicle.destroy', $vehicle->id) }}" title="Delete">
                        <button type="button" class="btn btn-danger">Delete</button>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            <a href="{{ route('vehicle.create') }}" type="button" class="btn btn-primary">Add vehicle</a>
        </div>
    </div>
@endsection
