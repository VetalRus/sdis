@extends('admin.index')

@section('content')

    <div class="container-fluid ">
        <div class="col-12">
            <table class="table table-striped table-dark mt-3">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Title</th>
                <th scope="col">Description</th>
                <th scope="col">Number</th>
                <th scope="col">Type</th>
                <th scope="col">User</th>
                <th scope="col">Commune</th>
                <th scope="col">Vehicle</th>
                <th scope="col">Image</th>
                <th scope="col">Created</th>
                <th scope="col">Updated</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>

            @foreach($posts as $post)
            <tr>
                <th scope="row">{{$counter++}}</th>
                <td>{{$post->title}}</td>
                <td>{{$post->description}}</td>
                <td>{{$post->number}}</td>
                <td>{{$post->type}}</td>
                <td>{{$post->user_id}}</td>
                <td>{{$post->commune->title}}</td>
                <td>{{$post->vehicle->title}}</td>
                <td>
                    <img  style="width: 50px; height: 50px"
                          src="{{ asset('storage/'.$post->img) }}  " alt="image post">
                </td>
                <td>{{$post->created_at}}</td>
                <td>{{$post->updated_at}}</td>
                <td>
                    <a href="{{ route('post.edit', $post->id) }}" title="Edit">
                        <button type="button" class="btn btn-warning">Edit</button>
                    </a>

                    <a href="{{ route('post.destroy', $post->id) }}" title="Delete">
                        <button type="button" class="btn btn-danger">Delete</button>
                    </a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            <a href="{{ route('post.create') }}" type="button" class="btn btn-primary">Add new post</a>
        </div>
    </div>
@endsection
