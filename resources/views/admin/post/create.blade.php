@extends('admin.index')

@section('content')

<div class="container ">
    <div class="row d-flex justify-content-center">
        <div class="col-6">
        <h2 class="mt-3 text-center"> Create new post</h2>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-6">
            <form method="post" action="{{ route('post.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <label for="exampleInputDescription">Post's tittle</label>
                        <input type="text" class="form-control"  name="title" placeholder="Enter title">
                    <label for="exampleInputDescription">Post's description</label>
                        <input type="text" class="form-control"  name="description" placeholder="Enter description">
                    <label for="exampleInputNumber">Number</label>
                        <input type="text" class="form-control"  name="number" placeholder="Enter number alarm">
                    <label for="exampleInputUser">User</label>
                        <input type="text" class="form-control"  name="user_id" value="1">
                    <label for="exampleInputType">Post's type</label>
                        <select name="type" class="form-select" aria-label="Default select type">
                            <option selected>Select post's type</option>
                            <option value="Alarm">Alarm</option>
                            <option value="Activity">Activity </option>
                            <option value="Driver">Driver</option>
                        </select>
                    <label for="exampleInputCommune">Commune</label>
                        <select name="commune_id" class="form-select" aria-label="Default select commune">
                            <option selected>Select commune</option>
                            @foreach($posts['communes'] as $commune)
                                <option value="{{ $commune->id}}">{{ $commune->title }}</option>
                            @endforeach
                        </select>
                    <label for="exampleInputVehicle">Vehicle's type</label>
                    <select name="vehicle_id" class="form-select" aria-label="Default select example">
                        <option selected>Select vehicle's type</option>
                        @foreach($posts['vehicles'] as $vehicle)
                            <option value="{{ $vehicle->id}}">{{ $vehicle->title }}</option>
                        @endforeach
                    </select>
                    <label for="exampleInputImage">Image</label>
                        <input type="file" class="form-control"  name="img">
                </div>
                <button type="submit" class="btn btn-primary mt-3">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
