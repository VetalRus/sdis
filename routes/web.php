<?php

use App\Http\Controllers\Admin\CommuneController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\VehicleController;
use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'homePageInfo'])->name('home-page');
Route::get('/alarmes', [MainController::class, 'allAlarmPosts'])->name('alarmes-list');
Route::get('/actives-drivers/list', [MainController::class, 'allActivesPosts'])->name('actives-list');
Route::get('/single/{id}', [MainController::class, 'alarmSinglePost'])->name('alarm.single');
Route::get('/vehicles/list', [MainController::class, 'vehiclesPageInfo'])->name('vehicles-list');

Route::get('/dashboard', function () {
    return view('admin.index');
})->middleware(['auth'])->name('dashboard');

Route::group(
    [
        'prefix' => 'dashboard',
        'middleware' => 'auth',
    ],
    function () {

        Route::get('/commune', [CommuneController::class, 'index'])->name('commune.index');
        Route::get('/commune/create', [CommuneController::class, 'create'])->name('commune.create');
        Route::post('/commune/store', [CommuneController::class, 'store'])->name('commune.store');
        Route::get('/commune/edit/{id}', [CommuneController::class, 'edit'])->name('commune.edit');
        Route::post('/commune/update/{id}', [CommuneController::class, 'update'])->name('commune.update');
        Route::get('/commune/destroy/{id}', [CommuneController::class, 'destroy'])->name('commune.destroy');

        Route::get('/vehicle', [VehicleController::class, 'index'])->name('vehicle.index');
        Route::get('/vehicle/create', [VehicleController::class, 'create'])->name('vehicle.create');
        Route::post('/vehicle/store', [VehicleController::class, 'store'])->name('vehicle.store');
        Route::get('/vehicle/edit/{id}', [VehicleController::class, 'edit'])->name('vehicle.edit');
        Route::post('/vehicle/update/{id}', [VehicleController::class, 'update'])->name('vehicle.update');
        Route::get('/vehicle/destroy/{id}', [VehicleController::class, 'destroy'])->name('vehicle.destroy');

        Route::get('/post', [PostController::class, 'index'])->name('post.index');
        Route::get('/post/create', [PostController::class, 'create'])->name('post.create');
        Route::post('/post/store', [PostController::class, 'store'])->name('post.store');
        Route::get('/post/edit/{id}', [PostController::class, 'edit'])->name('post.edit');
        Route::post('/post/update/{id}', [PostController::class, 'update'])->name('post.update');
        Route::get('/post/destroy/{id}', [PostController::class, 'destroy'])->name('post.destroy');
    }
);

require __DIR__.'/auth.php';
