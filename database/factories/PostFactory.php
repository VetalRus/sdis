<?php

namespace Database\Factories;

use App\Models\Commune;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title,
            'description' => $this->faker->text,
            'commune_id' => Commune::factory(),
            'vehicle_id' =>Vehicle::factory(),
            'user_id' => 1,
            'number' => $this->faker->numberBetween(2),
            'type' => 'Alarm',
        ];
    }

}
