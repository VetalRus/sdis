<?php

namespace Database\Seeders;

use App\Models\Commune;
use App\Models\Post;
use Illuminate\Database\Seeder;


class CommuneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Commune::factory()
            ->has(Post::factory()->count(3), 'post')
            ->create();
    }
}
