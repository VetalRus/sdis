<?php

namespace App\Http\Controllers;

use App\Services\MainService\MainService;
use App\Services\MainService\VehicleService;


class MainController extends Controller
{
    protected $vehicleservice;
    protected $mainservice;

    public function __construct(VehicleService $vehicleservice, MainService $mainservice)
    {
        $this->vehicleservice = $vehicleservice;
        $this->mainservice = $mainservice;
    }

    public function homePageInfo()
    {
        $allEvents = $this->mainservice->getHomePageInfo();

        return view('main/home-page', compact('allEvents'));
    }

    public function vehiclesPageInfo()
    {
        $data = $this->vehicleservice->getVehiclesInfo();

        return view('main/vehicle', compact('data'));
    }

    public function allAlarmPosts()
    {
        $allInfo = $this->mainservice->getRigchtInfoBlock();
        $allInfo['posts'] = $this->mainservice->getAllAlarmPosts();

        return view('main/alarmes', compact('allInfo'));
    }

    public function alarmSinglePost($id)
    {
        $allInfo = $this->mainservice->getRigchtInfoBlock();
        $allInfo['post'] = $this->mainservice->getPostByID($id);

        return view('main/alarme', compact('allInfo'));
    }

    public function allActivesPosts()
    {
        $allInfo = $this->mainservice->getRigchtInfoBlock();
        $allInfo['posts'] = $this->mainservice->getActivesPosts();

        return view('main/activities-drivers', compact('allInfo'));
    }

}
