<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Vehicle;
use App\Services\AdminServices\VehicleServices;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class VehicleController extends Controller
{

    /**
     * @var Vehicle
     */
    private $vehicleservice;

    public function __construct(VehicleServices $vehicleservice)
    {
        $this->vehicleservice = $vehicleservice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $vehicles = $this->vehicleservice->getAllVehicles();

        $counter = 1;

        return view('admin.vehicle.index', compact('counter', 'vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): View
    {
        return view('admin.vehicle.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:255|min:2',
            'type' => 'required',
            'description' => 'required',
        ]);

        $vehicle = $this->vehicleservice->createVehicle($request);

        return redirect()->route('vehicle.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id): View
    {
        $vehicle = $this->vehicleservice->getByID($id);

        return view('admin.vehicle.edit', compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:255|min:2',
            'type' => 'required',
            'description' => 'required',
        ]);

        $vehicle = $this->vehicleservice->updateVehicle($request, $id);

        return redirect()->route('vehicle.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $vehicle = $this->vehicleservice->deleteVehicle($id);

        return redirect()->route('vehicle.index');
    }
}
