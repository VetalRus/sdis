<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AdminServices\PostService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;


class PostController extends Controller
{

    private $postservice;

    public function __construct(PostService $postservice)
    {
        $this->postservice = $postservice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $posts = $this->postservice->getAllPosts();
        $counter = 1;

        return view('admin.post.index', compact('counter', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $posts = $this->postservice->getCommunesAndVehilcesForNewPost();

        return view('admin.post.create', compact('posts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:50|min:3',
            'number' => 'required',
            'description' => 'required|max:255|min:3',
            'type' => 'required',
            'user_id' => 'required',
            'commune_id' => 'required',
            'vehicle_id' => 'required',
            'img' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $post = $this->postservice->createPost($request);

        return redirect()->route('post.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     *
     */
    public function edit($id): View
    {
        $post = $this->postservice->getByID($id);
        $post_setting = $this->postservice->getCommunesAndVehilcesForNewPost();

        return view('admin.post.edit', compact('post', 'post_setting'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:50|min:3',
            'number' => 'required',
            'description' => 'required|max:255|min:2',
            'type' => 'required',
            'user_id' => 'required',
            'commune_id' => 'required',
            'vehicle_id' => 'required',
            'img' => 'mimes:jpeg,jpg,png,gif',
        ]);

        $post = $this->postservice->updatePost($request, $id);

        return redirect()->route('post.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id): RedirectResponse
    {
        $post = $this->postservice->deletePost($id);

        return redirect()->route('post.index');
    }
}
