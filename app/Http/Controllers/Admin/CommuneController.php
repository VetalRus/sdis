<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\AdminServices\CommuneService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CommuneController extends Controller
{

    protected $communeservice;

    public function __construct(CommuneService $communeservice)
    {
        $this->communeservice = $communeservice;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $communes = $this->communeservice->getAllCommunes();
        $counter = 1;

        return view('admin.commune.index', compact('communes', 'counter'));

    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.commune.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:255|min:2',
        ]);

        $data = $request->all();
        $commune = $this->communeservice->createCommune($data);

        return redirect()->route('commune.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View
     */
    public function edit($id): View
    {
        $commune = $this->communeservice->getByID($id);

        return view('admin.commune.edit', compact('commune'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $validated = $request->validate([
            'title' => 'required|max:255|min:2',
        ]);
        $data = $request->all();
        $commune = $this->communeservice->updateCommune($data, $id);

        return redirect()->route('commune.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */

    public function destroy($id): RedirectResponse
    {
        $commune = $this->communeservice->deleteCommune($id);

        return redirect()->route('commune.index');
    }
}
