<?php


namespace App\Services\MainService;

use App\Models\Vehicle;


class VehicleService
{
    public function getVehiclesInfo(): array
    {
        $allVehicles = Vehicle::all();
        $type1 = Vehicle::where('type', '=', '1 ')->get();
        $type2 = Vehicle::where('type', '=', '0 ')->get();

        return compact('allVehicles', 'type1', 'type2');
    }

}
