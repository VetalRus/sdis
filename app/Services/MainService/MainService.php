<?php


namespace App\Services\MainService;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class MainService
{
    /**
     * @var Post
     */
    private $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function getRigchtInfoBlock()     //Возвращаю данные для  правого блока с информацией о количестве и
    {                                       // частоте событий с архивом
        $allInfo['archive'] = self::getArchive();
        $allInfo['count_posts'] = self::getCountPosts();
        $allInfo['count_category'] = self::getCountCategories();
        $allInfo['difference_days'] = self::getDifferenceTheDays();
        $allInfo['frequency_of_events'] = self::frequencyOfEvents($allInfo['difference_days'], $allInfo['count_posts']);

        return $allInfo;
    }

    public function getHomePageInfo()   //Возвращаю  на главную страницу массив с данными будущих и прошлих событий
    {                                  // разных категорий
        $future_events = Post::where('type', '=', 'activity ')
            ->whereDate('created_at', '>=', Carbon::today())
            ->get();

        $old_events = Post::where('type', '=', 'alarm')
            ->whereDate('created_at', '<=', Carbon::today())
            ->get();

        return compact('future_events', 'old_events');
    }


    public function getAllAlarmPosts()   //Возвращаю на страницу Alarmes все посты типа alarm
    {                                   // этого года групировани по месяцам
        $posts = Post::where('type', '=', 'alarm')
            ->whereYear('created_at', Carbon::today()->year)
            ->get()
            ->groupBy(function ($posts) {
                return Carbon::parse($posts->created_at)->format('M');
            });

        return $posts;
    }

    public function getPostByID($id)   //Возвращаю данные определенного поста по id
    {
        $post = Post::find($id);

        DB::commit();

        return $post;
    }

    public function getDifferenceTheDays()  //Возвращаю количество дней с начала года
    {
        $current_day = Carbon::parse('yesterday');
        $yer = date('2021-01-01');

        return $current_day->diffInDays($yer);
    }

    public function getCountPosts()    //Возвращаю количество всех постов
    {
        return Post::whereYear('created_at', Carbon::today()->year)
            ->get()->count();

    }

    public function frequencyOfEvents($days, $countEvents)  //Возвращаю частоту проишествий(к-ство.дней\к-ство.событий)
    {
        return $days / $countEvents;
    }

    public function getCountCategories()    //Возвращаю каегории и к-ство записей каждой категории
    {
        $Alarm = Post::where('type', 'Alarm')
            ->get()->count();

        $Activity = Post::where('type', 'Activity')
            ->get()->count();

        $Driver = Post::where('type', 'Driver')
            ->get()->count();

        return compact('Alarm', 'Activity', 'Driver');

    }

    public function getArchive()  //Возвращаю масив записей прошлих годов с ключом года к которому он пренадлежит
    {
        $archive = Post::whereYear('created_at', '<', Carbon::today()->year)
            ->get()
            ->groupBy(function ($archive) {
                return Carbon::parse($archive->created_at)->format('Y');
            });

        return $archive;

    }


    public function getActivesPosts()    //Возвращаю все посты этого года типа active и driver  c групировкой по месяцам
    {
        $actives['Activity'] = Post::where('type', '=', 'Activity')
            ->whereYear('created_at', '=', Carbon::today()->year)
            ->get()
            ->groupBy(function ($archive) {
                return Carbon::parse($archive->created_at)->format('M');
            });

        $actives['Driver'] = Post::where('type', '=', 'Driver')
            ->whereYear('created_at', '=', Carbon::today()->year)
            ->get()
            ->groupBy(function ($archive) {
                return Carbon::parse($archive->created_at)->format('M');
            });

        return $actives;

    }
}
