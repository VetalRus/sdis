<?php


namespace App\Services\AdminServices;

use App\Models\Commune;


class CommuneService
{
    /**
     * @var Commune
     */
    private $commune;

    public function getAllCommunes()
    {
        return Commune::all();
    }

    public function getByID($id)
    {
        return Commune::findOrFail($id);
    }

    public function createCommune($data)
    {
        return Commune::create($data);
    }

    public function updateCommune($data, $id)
    {
        $commune = Commune::findOrFail($id);
        $commune->update($data);

        return $commune;
    }

    public function deleteCommune($id)
    {
        $commune = Commune::findOrFail($id);
        $commune->delete();
    }

}
