<?php

namespace App\Services\AdminServices;

use App\Models\Commune;
use App\Models\Post;
use App\Models\Vehicle;

class PostService
{
    public function getAllPosts()
    {
        return Post::all();
    }

    public function getByID($id)
    {
        return Post::findOrFail($id);
    }

    public function getCommunesAndVehilcesForNewPost()
    {
        $communes = Commune::all();
        $vehicles = Vehicle::all();

        return array('communes' => $communes, 'vehicles' => $vehicles);
    }

    public function createPost($request)
    {
        if ($request->hasFile('img')) {
            $post = new Post;
            $path = $request->file('img')->store('uploads/posts', 'public');
            $post->img = $path;
            $post->title = $request->title;
            $post->description = $request->description;
            $post->number = $request->number;
            $post->type = $request->type;
            $post->user_id = $request->user_id;
            $post->commune_id = $request->commune_id;
            $post->vehicle_id = $request->vehicle_id;
            $post->save();
        } else {
            $post = Post::create($request->all());
        }

        return $post;
    }

    public function updatePost($request, $id)
    {
        $post = Post::findOrFail($id);

        if ($request->hasFile('img'))    //если выбираем картинку в форме
        {
            if (!empty($post->img)) {   // если сущесвуе картинка то удаляем ее из локального диска и заменяем на обновленную
                $img = $post->img;
                unlink(storage_path('app/public/'.$img));
                $path = $request->file('img')->store('uploads/posts', 'public');
                $post->img = $path;
            } else {
                $path = $request->file('img')->store('uploads/posts',
                    'public'); //иначе всавляем новую картинку в папку и записываем в поле img путь для сохранения в бд
                $post->img = $path;
            }
        } else {    //Если картинку не выбрали при обновлении то удалается предыдущая картинка, если она была
            if (!empty($post->img)) {
                $img = $post->img;
                unlink(storage_path('app/public/'.$img));
                $post->img = null;
            }
        }
        $post->title = $request->title;
        $post->description = $request->description;
        $post->number = $request->number;
        $post->type = $request->type;
        $post->user_id = $request->user_id;
        $post->commune_id = $request->commune_id;
        $post->vehicle_id = $request->vehicle_id;
        $post->save();

        return $post;
    }

    public function deletePost($id)
    {
        $post = Post::findOrFail($id);
        if (!empty($post->img)) {
            $img = $post->img;
            unlink(storage_path('app/public/'.$img));
        }
        $post->delete();
    }
}
