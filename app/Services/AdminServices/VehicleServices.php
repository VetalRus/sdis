<?php

namespace App\Services\AdminServices;

use App\Models\Vehicle;


class VehicleServices
{
    public function getAllVehicles()
    {
        return Vehicle::all();
    }

    public function getByID($id)
    {
        return Vehicle::findOrFail($id);
    }

    public function createVehicle($request)
    {
        if ($request->hasFile('img')) {
            $post = new Vehicle;
            $path = $request->file('img')->store('uploads/vihecles', 'public');
            $post->img = $path;
            $post->title = $request->title;
            $post->type = $request->type;
            $post->description = $request->description;
            $post->save();
        } else {
            return Vehicle::create($request->all());
        }

        return $post;
    }

    public function updateVehicle($request, $id)
    {
        $post = Vehicle::findOrFail($id);
        if ($request->hasFile('img'))    //если выбираем картинку в форме
        {
            if (!empty($post->img)) {   // если сущесвуе картинка то удаляем ее из локального диска и заменяем на обновленную
                $img = $post->img;
                unlink(storage_path('app/public/'.$img));
                $path = $request->file('img')->store('uploads/vihecles', 'public');
                $post->img = $path;
            } else {
                $path = $request->file('img')->store('uploads/vihecles',
                    'public'); //иначе всавляем новую картинку в папку и записываем в поле img путь для сохранения в бд
                $post->img = $path;
            }
        } else {    //Если картинку не выбрали при обновлении то удалается предыдущая картинка, если она была
            if (!empty($post->img)) {
                $img = $post->img;
                unlink(storage_path('app/public/'.$img));
                $post->img = null;
            }
        }
        $post->title = $request->title;
        $post->type = $request->type;
        $post->description = $request->description;
        $post->save();

        return $post;
    }

    public function deleteVehicle($id)
    {
        $post = Vehicle::findOrFail($id);
        if (!empty($post->img)) {
            $img = $post->img;
            unlink(storage_path('app/public/'.$img));
        }

        $post->delete();
    }
}


